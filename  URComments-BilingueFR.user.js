// ==UserScript==
// @name           WME URCom NLFR_Belgique
// @description    This script is to facilitate the handling of URs in French and Dutch with the menu in French. To be used with the main script URCom.
// @namespace      https://gitlab.com/WMEScripts
// @grant          none
// @grant          GM_info
// @version        2023.07.03.01
// @match          https://editor-beta.waze.com/*editor*
// @match          https://beta.waze.com/*editor*
// @match          https://www.waze.com/*editor*
// @match          https://editor-beta.waze.com/*editor/*
// @match          https://beta.waze.com/*editor/*
// @match          https://www.waze.com/*editor/*
// @author         tunisiano187 '2018 based on Rick Zabel '2014
// @license        MIT/BSD/X11
// @compatible     chrome firefox
// @supportURL      mailto:incoming+WMEScripts/URComments-French@incoming.gitlab.com
// @contributionURL https://ko-fi.com/tunisiano
// @icon			data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAOWtJREFUeNrtfQuQVNW19j7dM8yAvAURJIQZ3gJDlDijAxlQLCSCMSpEENDwG/+6lTj8QlVIJVZiMFX3llpFBPLfVAmWgBqJQQUVAQ1PBR2IXB0eKgwzgDwFBgaQYWa6+9yzOt3jmT37dc7e5/Tp7r2qTvX7cR7ft7619tprG0hbUM0I2HeZCv+PqU9v5l1k2lJzLoJ0Dk2fP6dNE0DWHH8jgOfU9ADcmgw0AWT98TY8eE30/SoBayoAuiYETQBZC3pDwXtVnGs3QHbyvCYDTQBZd1wNhc+pJgTTBRBNRe/xKrTQpgkg5cfTDZgNl695dX55wDYFAW+6+G5NBJoAMh70vPuG4G8bkudcVvbL3HeiFjQRaAJIC+CLgJ71HI9IDAe/zfr/orJcxuubmgw0AWQb6EXAbXC+x8nnnBKCG8CLgNtkfJ8X5KCJQBOAr8fKC9DbnzM4zzklA1kCcHJLu88iD1OQcERyCpoINAGkFPj4c4bA6wbnMe012m/4SQCs+7zHImQgqwo0EWgC8BX4op6aBX7R5/HfkSEBJ/E+D/S852nEwCMBp6SgiUATgCfAF5HuNE9vCIA95JAARNSGEwJwAkge0HkbWIyhDExNBJoAUg1+Jwk50v2QIAmIEgBPSYjmBXgEoMLT07YYRgAmB/AqwgRNApoAmMfBSSaeF6ezgE177PR5URJwkwOggdApwEVeiwmQhOlQFWgi0ATgicc3BGJ6FtBDAu8TfT8vnyBKAjLenwXcGPZcTPC9PGLgJR01EWgCcOX1WRl8nsdngZkF6BDj/SEH6kCUBEQUgKj3FwE0iQRiDt7P+i0eETgJDUxNANnr9VlJPRbwRUDPusU3w8Et/rsoxQQQE7iNUR7TvicmGDIgAWLQaiBLCUDU64uOvdNkecgh2FngD1O+U1QROA0DeGPy9ix9TJAEYgKgTz4fZRAGiwx4RMCrO8hKNWBkMfBZXl90jJ4l2cMEeR9KPE8CfNgBKYQ44QSPBNwQABKM8WMCBCCzsQgB/10kQAZZTQI52utzk3j251nePsTx8mEGAZBeNziE4YQAROoCkEPg82J0kS1KuU96T1RQQRjYrUl5TAO6QQC9kalEYGQx+EWy+jxvbxAAywN8GHuO9L4wQzEYArkCNyrAlCQBEYlvB3WUAfSowGeigiGGSVAEbkYNMlIN5GQh8N16fZ6HJwGaBOQcCvDDAmTACw1YJBDfn7Kysq4/+MEPupimSSV/wzDMd95550RNTU09BSgxARIQJQASsEmvJY+F/XHyPYbtcXIzbF4/+ThmO688FWC/FSFMrQDSEPy8wh0niTscyDSA5xDUQFhAGYRZIwYFBQXXTJo06YYBAwZ07dOnT9eePXt27datWxfYscLCwn6yB7W6uvoQ3J49e/b8yZMna48ePVp78ODB2sWLF1cJeP8oQ9LjgI8QyID2PIlATIKCIJGUSJIw43MDRhaBX9Tr87L2OBBzKB7czcYikOZt6tSpvUtLS3sPHTr0Bgv4vSyw98rPz2+bqoN+9erVeosUTlRWVh76/PPPj2/cuPHYtm3bznHAH6WQQYTyOu35KCdsiDHCFBMb0UAORgwyggQMDX6i1w8JZu+THp0E3hwKyHMcEEH89sEHH/zexIkT+9988839wFIJdiekcMiy3bt3V7333ntVK1eu/JpBAqwtwnkcZRBLjJAziCJ+ebLopKO0JwEjw4GPkNh0WjexfZgDcN4tdbO8evsnnnhi+B133DGsf//+hekAeJ5duHDh/FdffXVo/fr1e5cvX36wpqbmMgW8EQrImyhEEOEQiKgqEJ2yjDhJU00Aae71Wdl4EcAn7+dQXsslfUdZWVm3Rx99tGj8+PE/vP7663uhDLe9e/fu27x5854///nPezAyiAjcRgjEELGBO8IgAhIp8HIDGasGjCwAPy/Jh3t/WiY+hwL+HIqXzyG83oIAwNPPmTOnyJL3t6hI1KUzGbz55ps7n3rqqUqKZ49gCiBC2EifYYUJrEIjmZBAE0DAwE8b3qNl1WlZ+hyK18+lkEEuhRxyIaafO3fumKKioqGZIO9V5g127Nixa/78+Vu3bdt2lgBuEuAbKQqBRQYkJRClhASxTCYBIwvBz4vzwxSvn8MAei7h9VYEsHjx4pLJkyePyQaJL2tWWHDo5Zdf3mapgs9tCoAE8iaKIqCRQIRCArShQ5wEEOKXE6cNERhZBH4D0Yf18EIdkucneXTY2lAkf/z9lrRvv2DBgrGWFXfq1KmLhrYzq6urO79ixYoNs2fP/pgA7CYCATTZXqMRAYkE7OoAHzIUTQ6mHQkYaQx+J5l+EeDjcX2Y4dHx+7m2UCD+mhXfd7SAP2bChAmjtcyXNxhFsBQBEEEFAfBNBEXQyMgXsEYOaMOHZiaSgJHB4A9hkt+gxPYhTlLPDvhcAgG0sT+2PH4H8Ph33XXXjzTwPScCO/ibCARAAn4Too8w8JKEGUcCRpaAnxbr0zL6pK0NhQiSIUDuokWLSh577LF7NfD9IYKFCxeu/uMf//gZQQHgm50AGhF55CCC6NWJKkggkAQQTvOY3yn4acm8XAKo7dIeHuclbttgj/PKy8uHrFmz5tFx48bdlpOTk6vh6b0ByY4dO/amRx55ZOBpy/bu3XsZtZwIhVDrsm7adaPSMbppw64VgALwhxA900+acZdLiPlbSXqMBOykAHK/owX8h4ZZpiGZWtu0adM2S32tq66uvpTw+o2EEKHJpgBoyUNcCUQQvYIw7ZVAOhGAW89Pm2iDZ/VzUcusfi5BBTS/tmLFitt/9rOf3ZWXlxdYuX+1KYJOXroSv3/i4rfxx0k7CY8j0daeNSeMena85rvHuTmoV+JxQdeOgSaBhoaG+meeeWblU0899T8YAdiJoIGSNGwihAYkAohmUjhgZAD4Sc04ScN6IUJiL0yI6/OwkKCNnQDKysque+mll6YGqXLvfH1DHNDJDYBdfa7Os98rvLYT6tw2D3WxNrjfs0O7OFEExaCy8N57733VUgMXCSTQSFECpKShaIIQIbEFTQJHAkaagR/3+jTPjwM+hMiZfRz8bTCpb7+ft3z58rGW1x+f6iQfgLy69iKqsUAOt3bPnioDMgDlUGARQqGlFOwqIiXq5+rV+meffRbUwGeYGmigJApZowYisw3TkgSMDAc/K8OfS9naYF4/NxHrT7dC/aGpkvL7T9fGt6AAnpuksxTBjT26xskAblOlEHbu3Llr2rRpb9rUQAPm/RspOQPSsGHGkYCRxeBvQyGAPDsBlJeXD37uuedm+R3rg6wHwO8+9k3c46e7AQkkN7/J4NSpUyeeeOKJv/39738/YlMAjZj3b0CthxGjqPVcA1qHIpEeAyTQawJgEIAs+EkxPk4A+G3zcN/q1avvtmLJ8X4eBAB80ttnqt3c+7pmMvAzJFiyZMma2bNnb6d4/Qbb4wii1xFEEXtOgZOqwZSTgBFg8JOAzyvtJRX34LE+SerbH+dZkr/T22+//dDQoUN9Gd4Db7+j5gT69PiZtJD3KvMGQAaj+vb0TRXssqy4uPhlGwE0EgiAlxtgFQ6xFksJXChgZBD47dl+vIKPBP48DPhxMigrK+tpScVZfszYq7Hi+U8tjw9eP9sNiGDcgO/FScHz415Tc+jOO+98sbq6us4G8qsY6JPAb0DkGYdNiD1MKLq6cUpJIBxw8CNEHuojderJIYAfB30LT2/b4s89+OCDBW+88cbszp07d/Ua+G9UVqGNB7/OiPhehcFx2HH4ZFwNwQhCWw8VQZcuXbpa53rIMcv27dt3CbGXS6PV9TsBq8nJb6XMgkQArKQfaRVdXsIPH9rLQeRS3vj9RYsWjXruuef+r5elvHbgw4WuLXVE0L59+w6TJk26qcqyBAkkrzlTATDtHl5kCfqUKfJwQMDPk/72pB8t2UeauUfy/MktP6kALPCXlpeXT/Uyxl/7xWG0dn+NBr5DIoCcSJ/OHVBOOKT8N4DsgQR69OhxZd26dccdKAADic/44xFKSucMhAMKfoT4FX6kZpukmXq0La4AVq9ePfHhhx++14udhIt3a/Vx9MqnX2qp79K+vnAZVXx9Ok4AQARekEBJScmwbt261VkkcFIAgKQhPSdrB5gM9es7CQSBAGjSnxb3s7L9tGQfkQAqKipmjBs3rswrub9s1/6MHs7zyyKxGDp45kL8WAIJdMhro/w3iouLhydI4AR2fZqCcb3b96Q0DxBOMfhxr0+L++1JvxxEHuYjDem1kPr2+5988slMi/lv8cLrr/zsANrw1RHiZBtt7u1yQxPaefS0Z2EBkMC111570aYEklI/RLh+TcUATokKCAcA/Ky4nyf9SQk/UlFPi1vw/F6AHzzUkop9Wu77EBYcOHvBEzUA4YBFAkklwJLwTlYUJr0/EGoglQQgGveT2naRJvXQkn32ob48iPm9kP2Q4INEH8hVbf6pATCYkeghCfDG7EWfZ72XFAr7QgjhFIFfRPqzqvxIM/poyb589N1QX6nqhB9k9ZdaXl/H+qkxyLXABmXFKkMCGwkcowA2xlAFpCShqQg3GUEAotKf1bSTBH680KeZAGCcX/VQH4B+2a4v0AU9tJdSAxKGkYKB3TsrDQlGjBjR79ChQwf27t17EQO0gVpW9iEBwOMkYRC+0/dQIJxi8CNEb+rBkv6k+n5a3748qPCDIh+VOwTFPGv2VmvJHxCD8wAhQbIvgQqDIcJ77rnn5qqqqoMWCVxy4PFptyI5A18LhMI+g58m/WmVfizw4xn/fELcD+Dvt2zZsl+qrPBbVVkVL1LRFjwDVQajBAO7d1FGAqNHjy546623Ks+fPx9heG3kUOqbLnDjOTiDJP1pq/OQhvvgflsc/IWFhZ23b9/+uKqJPXBh6Sx/ehhMLppc1F9drqGm5pB1PS1G/54cBFs9+q6PgL3HAEwqIi1YQlqfEJ89GCMQiae9A0IpAj9LEbCG/8KcfEALZQBTejX4s9NghuXijz5XNr26oKCg38aNG+/HrjE8HLX3msQXn8EXqKEtWuurww6niAB4BT806U8FO+79YbjvRz/60W0qdgJA/8rurzT408xgqBDqBUb07KZkhMAige9Thgd5zT94C4umTKWHfQI/z9PTYn/asB8zBwALdcyZM2eqKvCD59eZfk0CYLaRgTrE7/7DIwVS3sDXYiG/CIDn/Z0s2UUa8mseAYBuPmvWrPl/KpJ+SfBnU5ceTQJsSyQF+7711lufnT9/vgnRG3+INgQRAX/ajQI4TfyJjPfjSb88bAMFkP+vf/3rP7p3736dipgfCnzg4tGmScBu0Etg+PDheStWrDhAAC0+LiwKdlJtAA38RpAJQKTenxX3i3h/Urlv/vLly28fO3bsbSrAD57/zLf1GjkZSAIlfa5Xkg+wFEB1RUVFLcf7x5DYykE8JeCJCvBrFICW8RSp/uOt3htPDsKKPbBohyrw64RfZhqcV6jjUGHPPvvs/4E1Iwh5KXy+Cm1EIITo09954DfSgQBopEAjAVotAL6GX6sNlutSsWLPu18c1uDPcIMhQpi8JWuwVsTbb789HdGHBkVJIITYbfA9UwFhD8DtJPanSX988U5i3/7kfSsWG2dJ/1LZPw/lvbrCLzsMphSrKBu+zrJEKHAuId2jlDDAyWKipFDAExXgtwJgdfulzf7LIUiqZuk/ZsyYHrBKr+wfgxJSIABt2WOq1N7TTz89LREK4PUq9utWVAEYBLx4pgLCisHN8/605B/e249b6JPc3n333em9evW6QTYuhEIfPbEnuyzZamxk7+ukRgYg9Ozbt29s5cqVhxC5rBch8gIhvGXD8OeUhwB+KgASs4UoG20eQIvSy/Ly8kHDLJP5U5D0g6SQHuvPToOpxCqSgrCEHCSiKTkAERXASgayVIARVAJgKYIQJx9Aa/3VQmL96U9/mqYi7tdJv+w2CP+2K8j9LFu2bBqijFJhoaz9WjcQPRnoidf3ggAMjqenkQDP+5NyAPHbFStWjO3UqVOXIJx4belvMCog6whgwtDs2bMHM1RAWFAFsIbMlSYDvVIAtHkAIqMBpLxAC0YtLCzsMGXKFKkx/6T016YtaSquh9/+9rf3UZQrfk3ba15Iw+A0+a9UGYR9BDup0CeE2E0+ia2+XnrppfFW6D9Y5k9D624t/bXZLVn2LdNkFMqEu3XrdiHRS9A+BEia/09bPZjXPUhZWKBaAdCYira6L6/jb6v5AJbM6njXXXf9SFb66yae2rzKCc2cOfMu1LJxTS4lFyDSMwAhD5OBXoQAIjGMCBkQy38XLFgwRqbiD6S/iiowbRmcD/jisNTnO3Xq1HXx4sUlFMfGywGw8gGBGwYUYSBWtx+DA/4WJGDF/u0nTJgwWuYPb0+sPKtNG82qz9XFy4UVqACe16d5f1a3IKVk4MdkIJrHp40E0EYAwpb3Hyvj/QH4utpPm4hBlaBMbYhNBeRQHFuYoH5ZyUBWiO06DAj5AHineQDSgYoTgeX9pVb00dJfm5NQUXaIePLkyWMQeQar2zBAuYUkwS7KTEhQAdAkUhjYFGZfuf2zsHqMTvxpc2KgFmXCRWhIO3v27IGs6xq1HhVzUg8Q2MlAtMo/0Sm/rbL/CTaVOpnatPl93UyfPr0EywXQCoOSeHCTCHQdBoQ8Aj0SlP8ilYDh8vLygTLtvcH7Q2JHmzanBslAGRVQXFz8w7Kysm5IbPhPFPiBmgvAW/aLNsmBBHoiAcyYMaNYe39t6aoC5s6dWyIg/cOcPABNXackBDAcKgERz08EPwz9AYtq768tXVXAmDFjbkHk5DYN9DwVoCwM8GIyEE/+8xqAtNieeOKJIpk/tr3mhL6CtUnbDonrqHPnzl2efvrpERylKwP+lIYAiAB2xCEAGuu1GgqcMmWK66E/YG2d+demwj49fkaqLmDChAlFiJ35Fx0OREjhaEDIJdB5UkM08cccHoHkiUzyb4f2/toUGYBfxpkUFRUNQ+xRACfTg0WUgOEVAfAIgSf/efF/8/1HH310uMwJA9bWpk2VySQDoYZl/vz5RUh+LoCB+GX3vocAhsM8gFAicPz48be4/UPJdeK1aVNlEFJCUlkiDBhOyQE4CQOkAK+SAAwBMhDp/0/y/iFZ+b9be39tXuQCJCYJjRgxYhgD9GHkbEagkslBIUWgZ4UDtFqAEOMghB944IEBMkyth/60eWEyeQAIA55++ukiRJ4DI9IgVHm/QK+mA4sM/zFJ4I477hiWipOkTRvLZJOBo0eP7o/YowCiwDccYtKzHICo5Md3krUoaHjYsGFDXct/ybnc2rR55WCGDBnSH7GrAp3UBEirAK86AvGUAEsBhGbPni0l/3WvP21BJQDIa40ZM6Yb4lcAOgG/6yIhlaXArIUMEAX8BkX+99fyX1umhgGTJ0/uj9hZfwOJDwNKVQiGJEHPijsce/7ka0VFRa4JoEYn/7T5YDLXWXFxcX/EHvIT9f7S9QAqhwFFSYBLCrDAglYA2jI1DOjTp88NiDwRiJYsR4hdCJSyhUFYUl90IlCLx1OnTu3tmpUlijS0aXNikGtyO0MQ8gCFhYXtOEpYVdxveEkAIuRAYjRqGHD33Xe7lv967F9buoQBkyZN6u0yD+AkHPdcATjx+kJ1AIMGDXK91LdWANr8NJnRpkSiO8SR/k7mA7iqBsxREPuL5geEyKF79+5d3R7UE2k4/NelbR66ufd18eWoenZoh/Jzc1oQGuzTF1a86YW66dnxmvhv97JuC7p2bH4estwnL12J/ybEul4Mq97Yo2t8g/8AG/7b+0+di/92kNdwqJZwOD179uxKcYJuRwBM5BGYERJb6jt5S1veKx/Z1vdLbPAcxEJtE1u+aZrPu2XjxR99njbAh4t+0o0FLYDHiznfqKxSQgRANhOH9G0BPJ6yelfB6rlgQDjjBnwvTnwiBkVdst15vbT/vLvU1edOnTp1wiKBZ6y7cFDrgftsW0PiucbE1pTYIonbaOJ+cl1B0pqDiHCrRAEggYQEb4SAKHfKysquTYUc89sAALA5VQq/KBkaB4TMohWTi/rHQejEgKTKR4+IA9HtlFhQNjNHDhYmPDthgFKAfQ5ihSeQo9N9AktMdBP1/Ah51BnIq9mAIrK/VdXTiBEjXBNAuiz3BQB0Cn4cEI9ZRGAPFUQBCCB2Cn6cuCa7KNGA34b/7AYoyc+7IS4/7PyVq64/azm8rkis8s8QdLy+EYDI0AOrOQhpZ0O33HJLRicAJ1qSX8VFDNIdvKlT4hGV/DwCgv1wCn4Vvx1EEpBxPAmHRyuQE1EEvDCdi9dULQ9OfL5jx47u1/2TYGI/DOLuUX17Kvs+8KaiSgLeBzJalcF+iH6fKuJJ2qQhfYXzB36FAG4t4fBE8YGQB8uEqWwJxotVuNnNoqIi1xWAQQ8BHnBf3cwEIg8M8LpK4rGrGRHSU0k8zeHAiAGBOa8yjifh8JzKfsTx9CmrAxAhByYRmKaZkeAH2eqF1wIwlBawmybB607zBSKWHL7kKQ8vDNSPSlWRqhCgf//+vZD7NQCVJANDPhwjkRmC8a2rZW5+4ELACUC1F7TbyBu6S73u1X4BQbhN+omSalDM7YhMu3btRBSAIYgjXwjASfJPZHJQi+dgAQU3O1Ef8OafXhIAeHeaN4TnvfD+IvtVYMn/dD2mTg0Kl9xYQvGKTqqT9vZuCcBQQAi8XIDcCQhwDUChx0CI/wbF0/ohk2n753WiLkiJQNfHrrCwH3I+/k+L9w0X+PQlBGAxWPPzBQUF7ZA21yogaCDxg/gCkweQG4Fyk/gL1CiA0ziEyHD33HNPLw1lbU6srYfhjSMCUJuDEkn8oSASAG8HDK9OgO4BmJ0W9NoPF5hhKWdP8OPF0mA8b0/8vGmarnfwaiQa2DPrR4KS5oFSOTzqR2+GdCn/lsCPp45TlgAMBTtuoAw3P9QJ7Tf8+G0a0L3+7UxRfdOmTesliavA1wH4QSaBNi/7FLLaoMPzXnpJ1n5Vezw3ozpDmr9cd911bVOJgRDS5rntOHzSs+/mTZH1cgot67uhOMbL39ZLv2cWAZiZfJBBJnsxWxFAtp1DLvC6F6skg7rgKRuZ5bR5xJMh8b8mgGwx6Kij/DsFGoPA66sqq5T/tsh3Aki3K1Y/sD+w35li33zzTT3mBM10IQDThSc3sftmthAAeEyVQAQvKCqxwVOrlOOwH6JJuLWKWokl7ZXdX3miaFJlr732mmwsY6aKAET+gCkA9H8XRBuG6x3JzwmnxckGEKogATffA+9XQQJuvmdJxT4lJLBKUU/EgJnJ8P6eO8mQ4h1hgd5k7JTUTgalJFQUvK98+qVrLwZxtVsSgc+tdRmKwP+F/+2GROCzQAJuwwEIJaDha4av+sxSx54RgdMW37yOwKRuwLnou07AcJuPvusIDBugt21BQUGX6urq/3QLCq8STl4Z1O9DdxuY1SYyY09ld1yYIwBz9UWm1CYXwpRpRGo3mCMAvy0yVTiZ5PQqkanKZFqVWcp3NvquK7C9OzDch5PdiFp2B47Ytmhic90ZOEeQmQxJZuOxmVlTU1OPssiSyTkAP5AAzOjr0i6/pee7crU5264y6w3fBb8NhJLszU/6bRhrh99WCT6Q8LABCcFvw7RhvKYf1kKoSaxJkA6GHzvha+Dq1XoKUFlq2hQMrYWUtexsCh45iMgaaWmTTiEAiQicJPRUmhdZeqe/narfD4KdsIwDdCdgdoWjkAvAi3h71s6YNJa7cOHCeTc7EZRZYdqy02BFJ5fyHzG8vcmQ88rMjzoAXvKveau1zM0PdM6A5hDa0tfcdl06c+ZMLRJP+JkOQmrfCcDkKAESq+EJC7PeMlcxmCYAbany/hLh58mTJ8+jlgm8GAU7JkNtS4UCKusAWDKFJW2aCaGqqupEKk6ENm1uTSb8PHr0aC0DD7xNSR5AdQhAy2QikZ26ePHilVScCG3a3JpM52PL4Z1zAHYnSUHTawIQ/XER8DfLnl27dp1IxYnQps2tyYSfn3/+eS0S8/KsRKFIHsD0QgHwpIjIZl/e2KysrDynQwBtaUUALmsAwLZu3XpOIPb3tBxYdRJQhhBiiQPiOxNr0+a38qypqTkkCHxPSSHkEOAir4l4/RgiZz9jiQPjSgHk6zyANh9NRnUmhgBjjE0U+FIJQTcKwFRIBDgpAAEcd3tQe+kwQJuPViiRdzpw4MBxghOMIfowOQ1bwmD3KwTgjVvScgDx+/v373dNADoRqM1X+S+x+MmuXbuOM7x9DLvlYQu5DQu8rAR04v2bSWD79u2uCcCP1Wi0aVMRAixatOigS/kvO2dAKQG4Gu+nSJ/4tnLlymOJWVKuFIDOA2jzC/xuE8+2BCAN9DGHwHddE6CqJRiv+QcJ9FQiOHnypOt6gEIdBmgLePx/+PDh4wQsuFUAjkHvlgBEGEek5De5RSkqILpnzx7XfbOCtGy0tsy1G6+/1vVnP/roo4PY9R9l4MHTGgEvWoIhJJjwI4Efbjdu3HgwFYkZbdpEDMJMmYTzsmXLDtqvdweAFw3HU0IAJPAjJDD0h2+QIHGbB4C4TFcFavPU+0uoTIj/q6urv0X8GgCRWoBADAOKdDU1RYBvZ8Wqqqpqt3/IbX82bdq8JoDt27fvEQC9ikIgXwhApOuPk1GApCSKbt68eY/OA2gLovyXub7Wrl170BbzRzkkEEP8vhqimFRCAG4W/mAVOJCSIPHbVatWuU4EJhtOatMWJO9fV1dXu3Llyq9xZyco/13H+V4pADfenyX7W2zbtm07c+rUqROpOFHatNFsVN+erj+7e/fuPaRrnaMISPkzhMQrBH3JAdD+iAgJRGlk8MEHH+ySyQPooiBtKg2SyzIJ5hdeeGEnA+y80QBEUAHSasDPUmCa/KeFAdGlS5dWyvyBkTd011ettkB4f5v8b3WdI/LQuClABFLxv1sCcFMQFENiCcAW7GiFAWfdTg8GKy3opa9abUpMNvmXkP8RwrUeE5D/bjsB+aoAREMAk+H5W5HBu+++6zoMgGSgHhLUpsr7y4SU8+fP35K4tiMC8t/pJKBAtgUn/UlW/TMxGfj8889Xui0KiocBmgC0KTAZRwIqduvWrWcRO/lnCuYBlAz/yRKA6JLgrAlApFpo+6KH8fvV1dWXKisr97o9+FCyqacJa5MFv0zLubVr1+5ErRf1jAjkA0wXRODIwhKfxVcNxlcOxrcQIq8qHMbuJ2+bt8uXL1+cPHlyqetQoF1+pi8trc1DmzFysOu28w0NDfWjRo1ajr5b6beJskWx8ICUE0BILBmYshwAQvw1zpwkA+OMCdlTmZoArQK0pcr7b9iwYRvm7Xlj/6RmOQh51CE4FR2BhIuB7ESwatWqrTJ/Btak16bNiUHSb9KQvlLfsWDBggoCAZASgVEkNvzHUwG+dQQSHQ7kkQEpBmp1wMrLyyvq6urOu/2zoAL0iIA2Jyab+d+5c+eurVu3nkGt81syw3/K5L8XCsBEYp1MRVVAi6SJdTB3yqoAXR2oTShvZMl+mcIfsHnz5q1neP8oId43kVxPgEDkAGRAbwd8U+J+8+2cOXM2ywwJqjip2rLDZJ1FTU1NleWwvrFdw02IPQrAGgJEgirA94YgopWAiJMPiHJyAc1Dghs2bPhQ9sTqVYS0sQwSxrLh4sKFC9cj9tAfSQGwvL802P0IAUh/khTDkIqB7AcjQrudO3fuFhkVADZ5xAB9lWuj2gNF/aU+D97fIoCvMAIgEQEOfpKDRARHilQRQshD8LNCARYJkEKCiEoVAAlBHQpo80ohzpo162+Y5I9wYn9aZ2CWIw1MDkAE9IgSBsQYYYCnKkCHAtpwg6m+ssPFuyyzxf4RxnVsJwHfk39JCyv6HoNw314FSHo+SUC0ysEwalkxmLwfPn/+fKxHjx5XSkpKhrn9wznhEOrVqb2uENQWN0j4PXbrMNcVf2DglB555JFlR44cuYi+q/qz3yYTgri6tatfOxkg5OEQoMoQQLQ7EEL0ngAk+W8fCbBvjbNnz/4Y5ljrUECbCoOCH1lF+P7772+zvP9p+3WKWo4ANCFyPQCvB4CTPgApUQAyKsA+PwB/jCsA+2ZcuXLl9I9//ONimT89sHsXtP90Lbrc0KRRkKUGGX9Z6Q/O6P7773/VUqffYsDHFQCpGtBOBibF+ysBvFcKQEQFIAEVEGPkAVptixYt+mrfvn17Zf84TPbQBULZG/fLlvuCWdfiakhQ265Pu+d3U//vZhpwShUATwWQXmdtIrMHDYsAjk2bNq04Jycn1+2fhrivT5cOOh+QhXH/TIv8YbaojIETmjJlytqEl2+gSP9GRB8RYHl/pYBPFQHQXkOY9A9RQgA8Odj8+pEjRxoGDRpkFhUVDZb54xD/wQVx8MwFjYwssak3DZSeJQrTfS0H9KJ1HdYlgN5AyFmRFKzTuQCmF2TgZR0Aq4SRVRdASgzaGbNVYnDmzJmbLfl1SPbPQ0JQTxjKDpt4Y4GS1vH/+Mc/NtgSf41YCBBF9BoAkbJfzy3swXcahMd4MjD5vIn4iUJWaND8/s8+++yIbCgABhfFyYvfojPf1muUZKgByU8Y9H3p74GKv7Fjx76GSX88BLAP/+Hen1YJiJBPpcBhj46xwSAEFjHwwE/NDSRDgREjRgyW/fMDu3dGB85e0CMDGQr+yZKlvknpX1JS8ufz589fsYG+kRACkKoBcQLgAd8zC/lwzFkNQ3lhAKs0GE+wND388MObZdqI25NDj5UM1asMZ5hBvK8C/GDPPvvs36yw86JArM+b/MPK/CMvvb+XBMCaGky7L1IcxCIBuG288847XwR21iSgzW5wHmfcPEjJd23atGnbH/7wh/9BLcf5IwQSEB368xTkqQgBWLkA2nMs+Y8oIUCr5yxJFrl48eIx2QIhMCgXHtGzmw4HMgD8QOYqaj2gN2VRUdHShNS3x/uNqHXTT9YkIFLJL2sBEE+IIezxsXdSHcgiAV5eoAUJVFRUXLjpppvCgwcP7q9JQMv+WbcMUQJ+UJY//elP/39iyK8BA30jIxxIKlcc/DT575sSCPl4LtyuGEST/o2ErfmkWCdqrYoqQR0OpK9Bwu8Xijw/2Lx5817cunXrScz7u0n8xSjeXySMTisCMB0QAULsacLUHgHYgW8mhJ/85CevyLQTJ5GAXnY8fcCvKuEH9sorr7y1aNGiLxC5p3+E4flpq2A7Xf4r7XIArFwA7z4rFxCihAd4fUAI8gHHLZs0adJNsvUByXCgqFc3dL6+IV4roC2YBsBX2QZ+165dO+++++41BMWJj/PbyYBW/8+a7uur9/eLAFhxPg/8vNwBN1ewd+/ey1WWyawshBuoACgdhlmE2oJjoNKgvBdIWpVBsc+wYcOWUICPg99eqRolKADeYh++VwOGfTw/vIlCImRAIwJ7ONNKGezbt+/Stddee1GmgQhukA8AIqg8eQ5FYjGNvhQbnA+Y2KNyBSgIH8eOHfvfiWIfPMvfSJH9TYi+4rXTWn/PQ4FUEQDpMYsceN6edT8eGqxbt+6EahLokNcGlfTpgb6uuxwPC7SlLt6f9oOB0rP6cPCPGjXqL9XV1Rco4G9E7OIfPJHNGvZDDsOBtCQAkVCARQys12lzDFq8BiRQVlZ2TUFBwfdV7RDkBZITiGpqL2o0+iz5fzqsMB7vw3lQZTDc98tf/nLphx9+SMr4NyLymD9r3N8eAiBEr/v3TfqLgs0vFUAb1096cHzV4BzbLWy5tq1NYoP70OMpP/E4L3lbUVExo7i4+BbVOweJwVc+/VKrAR8MpD6071bd2BXAP2vWrMWvvfba4QTwryL6kDOtApCVAyCN/SMkPhSY1gTAi/FZk37wZcNxEmhjIwM7CdgJoJkEPvnkkxlWOPBD1Tt3tSmCNh78Gm0/fFKj1COvDx7fi16OCfD/xQJ/jQ3kVxE/AYhP/6WV/5J6/qe0FNhI0XnkNQ4hlfziBICTQRLwdhJIEkCeTREkSaDtxx9//NCtt9460osdhHDg3f01ergwDbw+wfOTpveyuvzSFv1wkvzz3funIgcgGgrw3sNTD6zXm++/+OKLXxYVFYWHDBlSqHoH4SIt6XN93GN9feGyHimQPJawmtNdg74v1bablfCDmJ8A/kYK+O3AjyJ+wQ9Czgp+0rohiCwJ0F4zGURAUxK0/EKzwnj99dePdOrU6eJtt912oxc72adzh/hIARAAEIE2Z3J/TL8b4oU9XpVhJ7P9iYRfo0CyL4LoLb55M/48W+EnHQmARwKIQgKGw++lqYpkiIE2bNhwumPHjpcsEhjixU5Chhraj8NowdVIVIcFAgZxPgztwXFTmeFvEab9u6PPfyeG+hoQPeNPaupJq/aLOgR/yrx/KnMATvMBbpKCyXxAHpYYTCYE8205gfj9++67r8+rr77687Zt2+Z7ucMwSgCJQt2FuLXHH3lDd1Ra0MvzZdugvLe4uPgVG8ivEmJ80YIfUrMP02HSLyWKIGgEwMoHkAjATgQ4AeDJQBYBND8uKSm5ftWqVVN69/a+O2iSCKCkGEYPshn4kNWHzY/1GWBiz8yZMzeilhn9BkSe1tuAnK/y62ayjxkE8AWVBGj1ASQC4NUHJLe2JAKA5y3wd1q9evXEkSNH9vdj55/b/GlW1g5AXO9nJ2bI9MOUXtusPrvEpxEAb5VfUuyPEHuBj0CAP0gEQCMBGhHY1UCYQwJ5NjKwhwMkAsi3PZ+/YsWK0ZanKPZyp8H7Q/FQthhIe5hDAaD3s78CJPumTp26dOvWracQuYMPrZ+/aJsvVqFPoOL+dCIAkhoQrQ+wFwjl2u7TFECejQCSZJA3ffr0ghdeeOGudu3atfFip5dW7EPV5+oyXuID6JOb3wY9/MaNG/eGDei4l2/kxPpOe/w5KfU1gwS6dCAB2mpBYUo+gBYG5FNCgHZ2AoDbnj17dli1atXY0tLS3ip3FoqFlnyyNyNBD969sGtH3z09Lvmhe2+igWcjw9PjY/ykZF8MiWX70wb8QSQAmXwASQGEMfAnQwLqSACJAJLK4Jlnnhn6q1/96sZrrrkmV8WOZpL3hyq9nh3aoQLrFoCf6sVWYYjvzjvvXFpdXV2HvusUhcf49mq+RkRfjJaU5Wdl+9MC/EElAPx/iQ4N0sqE22ChQC4B9Pb712DAb7ENHTq088aNG0f16NGjbTZ6fwB6fk447tUhnofbIPVKBK8Py3XNnDnzn6hlmzjaPH5WrM8q8ok6AH9gkn64pcOa2MniH1IRkP01+0lIkgNpvQHac6wETvNJ7d69e66lAKSPGwz/Bc1ArtvH3+3NNQosrx50gyawlkJ7M7FWXyMloddAie+bGMAntfPmDe8FEvDpQgCsij8TUwckEkCIXYNNm4rJ/Lzl9XOXLl36g/bt20uFAJD5D5r0V91E00+rq6urXbRo0epErB9heHoSIUQReTZfDInN6nPa2NPUBKBOBZAeIyS2MjFCDruxvv/++6X9+vWT6jkFBT9r99do8Cuwq1ev1lvnZNucOXM2J5bpiiD6opyk7r1RRB/iE12+W6TDT2CVQE4agN7p+3hjrq5yEhs2bCgtKiqSrliBPgFBKvpJV/BDKe+vf/3rdQm5H2F4ezyh14h5etLCHbTpvHaws7L9aQH+dFIATgjBEbAJ91v1HfznP/85Zty4cf1kfwwmAQUp9p94Y4EnjTV8ivO/YYCdtVhnEyPOZy3gQWrnldbgTzcC8BL41KHGTZs23Xn77bcPVPGja784HJgDAF7frxJcRR5/16uvvlqxcOHCLxF9URhScg+X+KxJPCTgi8zqS0vwZyoBsBYYIYIcUQqMtmzZMmHMmDFKpgiD5w9C4i/dljnbuXPnrnnz5q23SX27ZCf14yd5eru3F4n1zWwAf6YrABHghwi38fsffvjhPaNHj1bSQjwo0l/lKrleGozlb9iwYduCBQsqLOCfoQCaVqpLmq6LE0CMI/dFW3ilNfgzhQAMBvhJZICD3b7F37N9+/b7SktLi1T8Ocj6r6qsSvlBggYbKpfL8sJqamoOrV27dmd5efknqHWCjjYdl5TRJ3l6GvBpnh8h8W4+aQn+dCMA0U5AJOCHBMAfLyf++OOPp9x66603qfrT71pxfyo7ACX76QW1kAfG8C0vvxPz9lEKAUQpJECany86rJcEfhSJN+5kjTCZaYSpjAoBeDE+lwxefvnlMSq7BMOQXyq7/kCGH7x+0CQ/gH737t17lixZsvO11147SvDUJA/eRJHztGw+q4Q35tDrZyT4M4EARLoJtYrvSeCfPn3692bMmHGfqj8G1X6pKviBWH/SjQWB8vog7z/66KM977333sGVK1d+zQBrhPJchEEUMQngmwokf1qCX1RWB+X/0SYDJcFM6gUA9/GegMnZgO2QrQeA5ZX+q2PHjteqSvotqdjne5svkPvg8YMwvFdtWWVlZdWmTZuqFi9efJAhx52QAAngEY7EZ3l8O8BFp/GamQD8TFAAhiBp0MKA5m3dunX3pDP4/e6pZ7erlh0/fvyk5eFP7Nu378SOHTuOvf7668ds4CMl33hkEOGQBOt7TEHgO5X8GQf+dCQA0uIfpASgSPwf3wYOHNj+9ttvH6cEDImMv1/g9wv49fX1DUePHv3m0KFDpy5fvlxv3a+tqqqqXb9+/YkjR458K5h0492PCr4nhsSKd6IEmU+bspsV8X4m5wBEvD6x4Oc3v/nNzXl5ee1UgB88vx8Zfz+lfm1tbd2sWbNWvf322yc4sTgNvBECMGlltyKv0bx7lOPtTRfAz2jwZwIB8BQCVxGMHz9+VLqAHwA/0tr8Su5Z0v5UaWnpcsvjf4vIw24imXfa5BoeyGMCz5uIXsQj4vFRNnr9TCUAQ4AUWm29e/ceHOSYP9k6G5pp+hnfW7H8Zw899NBaG/hZtfS8QhsaqGMcCc+T9DzQO+37gLIJ/EEnAIMDbkPg88x8wZNPPhlI8APYobce3Hq9Qg7J3njjjU2TJ0/+APGr8HgJOR6gSZ6c5+lNBvBF5D5C9DJeGsgzEvyZpAAMDuiJzw8YMKCb2x+EOf2qwJ/soJvqhpqQ0Z87d+7Lf/3rX6F2uRGRm2U0IWdFN06kO6vNNg/wLOAj5H5xjowFf6YQgOtaBkv+d3f7WSjycQt+6LUHcXwS+EGo1INx+3Hjxq04fPjwJcTumiMyjz7Kic15r5mI38/RlAB+1kr+TCQAU/A9rWSfYRiuT7JIws/eQTe5pULSc7w+dNF9/+GHH96C2PPpaQlAWu88U5AETIY3p0l7XnyvgZ+FSUAq0AXe49h+ceswtKPmRPx+0oMnu+hCf/ygT7kFg/Lcn//853/btm3bGURvqsGbZYdLfRHg0zx4jAP2GKJ34GX1gdRyP80JgNcOzGSQAdU7bNmyZf8dd9zxgJs/BJ4cWmqlo8F8+9dff31Dwus3IvFuOjgBxBwm70wBzy46Fx+5SOxp4KcRAYiAntYMlHW/+YLcvXv3N9l2wqG11tSpU9+0Qv5LiN0/rwnRp+aKZvVpgGaBXKQm301GX4NfZQItBf+PV9pLmghknwSUvN9iMtClS5f+0r59++6ZfqJB7v/ud797c+XKlUcRvYEmqaMObzlsWttsETmPkHhvfbfZfA38LMgB8C4W0pBS/MLdv3//v4qLi3+cycB//vnn1y9atOhLipcn9dRjJfwiiD+9lkUACLH76fO8vQZ+FhMAbVUgxPAyNJkav8hfeumlLZlIALA45sKFC9fbuujSgN5ISfKxlseKIrFl1pyU4oqC3eSQP9LAz9wQACGxxUFJYUAuFgY0b8ePH5/fq1evGzPhhFqKZs8LL7ywxQL+V6h1Ca9In3xWow28JFekg67IyjlOPbyTabka+BlMAHYSCFNyAbnY1gYngUmTJvV65513/pKuJxGy+jt27Ng1f/78zYkFM6KcmF6kvJeW7Y9xpL/oWnmyoNfAzzICEEkEkgiARgJ59ts1a9b8xLKH0+nkwSo5W7ZsqXz88ccrUOvJOiIddZ2W9rJaZ8c8AL1Tb6+Bn2EEQFIBdiVA6vEXpqiAnATgk4/zMTLI3bNnz+PDhg0bE+SDcerUqRMffPDBzhdffPFzy9ufJcTrtOaZpD75EUHw4112aMlVJ2B3Kuk18DUBCOUBaCrArgRybAqgxVBhZWXl48OHDy8LWEIv3lDTAv2eROtsVr+8COOW1msv5sDrxzhJPoTcVeNp0GsCcB0G4LkAOxHkUnICbQhhQfy2oqJiVipHBhJts/daoD+4bNmyA9XV1d+i1ll4mvcW6aobE5D6ooU9CKnrpyc6r0NbFhEALwwgtfzGVYD9Ph4OtMGeiz/+/e9/P3zevHn/4UeREHj4w4cPHwfAb9y48VhC2pMaZ/D657EkPas5p2iSL+YQ+Br0mgB8CQNoycAQgQBybGAPYyoAJ4Sc5cuX337//fc/YBFBN9mdgGz9CcsA7Pv27TteVVV1buHChQcZ4GNNtaWRgZPmnLROuqypuQg5WydPg14TgKcEgBB76a8cTmIwjBEAfht/z5NPPjls4sSJPxwwYMCQbt269aH90dOnTx+rt+zs2bO1p06dqoUOugcOHKh95513jltS/gpiz3enFSzFON7bfhtB/GabIjF+TBD4PAJwAmYNek0AUmEArgjChC1EAjiWH8DVQotQYtCgQe0tVdA3FosZpmmGLKDXr1ix4jjjmLImvfDAyCIDljrg9d6jeX3RnnpuJb8GvSYAz1UAIuQCaEOEOMDtxBAivE5dVRi17kZsChKAKUgAIu223HblcdpTTwb4GvQBsXSdDJScA2DaAGdf4ilku5gNTiwaSxyHWALgyTg6CfgIgURo4DcYFztr2qsIUEXlOy17z/L0vMy+TJMNDXhNAEpBz7vADOxxjKF4aCWrMRsh4OGD4REBmAyJbjKkO2uZa9bzqjy+Br4mgJSrAIOTgIoJfJcdDCEbyPAwAgd+iBCOiBCASQEqb3Ub0Q66XjTT1MDXBJAWKgBhCiCEkYD9gg4TABmyfS5sIwIaARicBCBC/A44MU6S0ERiFXmsmN5pEY/uqacJIO1UgElIHpJIwH6RhzGQ2AkgypH8IUJikgYY3oy5GOJ3yI0h5w01YwIeXkbua+BrAkiZCmAlBO2vxxB7iC5kI4KYjQDsJca0IUckqExMhyTAA7TTfnsa+NpaecdM+e+koUHeKsGsAqIQ4/0G4/d4IYAoETghCdkuulrqawWQMUZTAjRQhrBbexhgJ4eYC/CzFABSSAhum2nSPL4Gv1YAaa0CRJQAokh6kudnfV7kOJqSigAhZ15eRt5r4GsCyGgSQIJEILI5OYamAwIQeQ4huYaaGvjaMmZtQIPy2HT4PaYD0HtBAAg5r7v3qqGmBr5WABmlBBBHDfBeNxjf5YQAVMh1L7voavBrAsio/eERARKI6w3K99CIxxQkABp4kQKga9BryzoCcEICPEJgeXlD8HedNMNwsrClyuYbGviaALJmvwwFpCB77ERXtvGymaYGvraMJgBREhAlBd5rpOdNlyTg5rEGvTZNAJJE4IQA3Bw/0yUZOPmsBr02TQAu9lMF2A2F4FQx1VYDX5smAAVA9vJYqUzQacBrc2z/CweYTRik6QkqAAAAAElFTkSuQmCC
// ==/UserScript==

/* Changelog/Versionning
 [Versionning/Changelog](https://gitlab.com/WMEScripts/URComments-French/wikis/versionning/urcomments-french)
 */
//This script is to facilitate the handling of Dutch URs. To be used with the main script URComments.
//If you want to include quotes in titles or comments, you have to escape them. Example "Comment \"Comment\" Comment",
//If you want to use multiline there are two posibilities, use \n. example "Line1\nLine2",
//or \r\ at the end of the line and you continue at the next line (this technique does not work on older browsers)
//If you want to insert an empty line use \n\n. Example "Line1\n\nLine2",

//
// Afin de ne pas charger l'affichage, je ne change pas les textes qui n'apparaissent que pour l'éditeur.
//

// Nom du traducteur
window.UrcommentsNLFR_BelgiqueTranslatorsName= "@acidoxy";

// Configuration de la signature
var URCommentCustomSignature = '$SIGNATURE';

const URCommentCustomVersion = GM_info.script.version;
const URCommentCustomUpdateMessage = "yes"; // yes alert the user, no has a silent update.
const URCommentCustomVersionUpdateNotes = "Une nouvelle version du fichier de commentaires a été installée! v" + URCommentCustomVersion;

if (URCommentCustomUpdateMessage === "yes") {
	if (localStorage.getItem('URCommentCustomVersion') !== URCommentCustomVersion) {
		alert(URCommentCustomVersionUpdateNotes);
		localStorage.setItem('URCommentCustomVersion', URCommentCustomVersion);
	}
}


//Configuration personalisée : ceci permet l'automatisation de vos messages de rappel, ainsi que vos fermeture en "non identifiée" d'être nommées comme vous le souhaitez.
//La position de ces messages est importante, les positions se comtent avec les "," en commencant à 0.
//On compte donc les titres, commentaires, et statut de la demande. Dans cette liste, le rappel suivant est "Rappel"
window.UrcommentsNLFR_BelgiqueReminderPosistion = 9;

//Ceci est le message qui est ajouté à l'option "lien de rappel"(reminder link)
window.UrcommentsNLFR_BelgiqueReplyInstructions = "Pour répondre, veuillez utiliser l'application Waze ou vous rendre à l'adresse suivante "; //suivi par l'URL - tunisiano187 10/7/2018

//La position du message "fermer en "non identifié" (compte identique au rappel). Dans cette liste, le titre est "Aucune réponses"
window.UrcommentsNLFR_BelgiqueCloseNotIdentifiedPosistion = 12;

//Ceci est la liste des type d'UR par défaut de waze. Editez cette liste en tenant compte des termes utilisés dans votre zone!
//Afin que celà fonctionne, ces message doivent correspondre à vos titres de commentaires automatiques!
window.UrcommentsNLFR_Belgiquedef_names = [];
window.UrcommentsNLFR_Belgiquedef_names[6] = "Incorrect turn"; //"Incorrect turn"; A modifier
window.UrcommentsNLFR_Belgiquedef_names[7] = "Adresse incorrecte"; //"Incorrect address";
window.UrcommentsNLFR_Belgiquedef_names[8] = "Route incorrecte"; //"Incorrect route" A vérifier;
window.UrcommentsNLFR_Belgiquedef_names[9] = "Rond-point manquant"; //"Missing roundabout" A vérifier;
window.UrcommentsNLFR_Belgiquedef_names[10] = "Erreur générale"; //"General error";
window.UrcommentsNLFR_Belgiquedef_names[11] = "Interdiction de tourner"; //"Turn not allowed";
window.UrcommentsNLFR_Belgiquedef_names[12] = "Jonction incorrecte"; //"Incorrect junction";
window.UrcommentsNLFR_Belgiquedef_names[13] = "Pont surélevé manquant"; //"Missing bridge overpass" A vérifier;
window.UrcommentsNLFR_Belgiquedef_names[14] = "Mauvais sens de circulation"; //"Wrong driving direction"; A vérifier
window.UrcommentsNLFR_Belgiquedef_names[15] = "Sortie manquante"; //"Missing Exit";
window.UrcommentsNLFR_Belgiquedef_names[16] = "Route manquante"; //"Missing Road";
window.UrcommentsNLFR_Belgiquedef_names[18] = "Point d'intérêt manquant"; //"Missing Landmark" A vérifier;
window.UrcommentsNLFR_Belgiquedef_names[19] = "Route bloquée"; //"Blocked Road" A vérifier;
window.UrcommentsNLFR_Belgiquedef_names[21] = "Nom de rue manquante"; //"Missing Street Name" A vérifier;
window.UrcommentsNLFR_Belgiquedef_names[22] = "Préfixe/Suffixe de rue incorrect"; //"Incorrect Street Prefix or Suffix" A vérifier;
window.UrcommentsNLFR_Belgiquedef_names[23] = "Mauvaise limitation de vitesse"; //"Missing or invalid speed limit"; A vérifier

//below is all of the text that is displayed to the user while using the script
window.UrcommentsNLFR_BelgiqueURC_Text = [];
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip = [];
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT = [];
window.UrcommentsNLFR_BelgiqueURC_URL = [];

//zoom out links
window.UrcommentsNLFR_BelgiqueURC_Text[0] = "Zoom arrière (0) & fermeture de l'UR"; //"Zoom Out 0 & Close UR";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[0] = "Effectue un zoom avant et ferme la fenêtre de l'UR"; //"Zooms all the way out and closes the UR window";

window.UrcommentsNLFR_BelgiqueURC_Text[1] = "Zoom arrière (2) & fermeture de l'UR"; //"Zoom Out 2 & Close UR";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[1] = "Zoom au niveau 2 et fermeture de la fenêtre de l'UR"; //"Zooms out to level 2 and closes the UR window (this is where I found most of the toolbox highlighting works)";

window.UrcommentsNLFR_BelgiqueURC_Text[2] = "Zoom arrière (3) & fermeture de l'UR"; //"Zoom Out 3 & Close UR";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[2] = "Zoom au niveau 3 et fermeture de la fenêtre de l'UR"; //"Zooms out to level 3 and closes the UR window (this is where I found most of the toolbox highlighting works)";

window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[3] = "Recharger la carte"; //"Reload the map";

window.UrcommentsNLFR_BelgiqueURC_Text[4] = "Nombre d'URs visibles : ";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[4] = "Nombre d'URs visibles sur la carte"; //"Number of URs Shown";

//tab names
window.UrcommentsNLFR_BelgiqueURC_Text[5] = "Commentaires"; //"Comments";
window.UrcommentsNLFR_BelgiqueURC_Text[6] = "Filtrage des URs"
window.UrcommentsNLFR_BelgiqueURC_Text[7] = "Paramètres"; //"Settings";

//UR Filtering Tab
window.UrcommentsNLFR_BelgiqueURC_Text[8] = "Cliquez ici pour plus d'informations"; //"Click here for Instructions";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[8] = "Instructions pour le filtrage des URs"; //"Instructions for UR filtering";
window.UrcommentsNLFR_BelgiqueURC_URL[8] = "https://docs.google.com/presentation/d/1zwdKAejRbnkUll5YBfFNrlI2I3Owmb5XDIbRAf47TVU/edit#slide=id.p";


window.UrcommentsNLFR_BelgiqueURC_Text[9] = "Activer le filtrage des URs"; //"Enable URCom UR filtering";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[9] = "Active ou désactive l'automatisme des URs-Com types."; //"Enable or disable URCom filtering";

window.UrcommentsNLFR_BelgiqueURC_Text[10] = "Activer le compteur des bulles-infos."; //"Enable UR pill counts";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[10] = "Active ou désactive le compteur des bulles-info situer sous les URs."; //"Enable or disable the pill with UR counts";

window.UrcommentsNLFR_BelgiqueURC_Text[12] = "Masquer les UR's en cours."; //"Hide Waiting";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[12] = "Montre seulement les URs à traîter (cache les URs qui sont en cours de traîtement)."; //"Only show URs that need work (hide in-between states)";

window.UrcommentsNLFR_BelgiqueURC_Text[13] = "Montrer seulement mes URs."; //"Only show my URs";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[13] = "Cacher les URs sans commentaire de ma part."; //"Hide URs where you have no comments";

window.UrcommentsNLFR_BelgiqueURC_Text[14] = "Montrer les URs nécessitant une fermeture."; //"Show others URs past reminder + close";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[14] = "Montrer les URs au rappel dépassé et que d'autres éditeurs ont commentés."; //"Show URs that other commented on that have gone past the reminder and close day settings added together"; //

window.UrcommentsNLFR_BelgiqueURC_Text[15] = "Masquer les Urs nécessitant un rappel"; //"Hide URs Reminder needed";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[15] = "Masquer les URs où des rappels sont nécessaires."; //"Hide URs where reminders are needed";

window.UrcommentsNLFR_BelgiqueURC_Text[16] = "Masquer les URs qui ont reçu une réponse."; //"Hide URs user replies";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[16] = "Masquer les URs qui ont été commentés par le rapporteur.";

window.UrcommentsNLFR_BelgiqueURC_Text[17] = "Masquer les URs à fermer."; //"Hide URs close needed";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[17] = "Masquer les URs nécessitant une fermeture."; //"Hide URs that need closing";

window.UrcommentsNLFR_BelgiqueURC_Text[18] = "Masquer les URs sans commentaire."; //"Hide URs no comments";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[18] = "Masquer les URs qui n'ont reçu aucun commentaire."; //"Hide URs that have zero comments";

window.UrcommentsNLFR_BelgiqueURC_Text[19] = "Masquer les bulles-infos sous les URs sans commentaire ni description."; //"hide 0 comments without descriptions";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[19] = "Masquer les URs sans description ou commentaire."; //"Hide URs that do not have descriptions or comments";

window.UrcommentsNLFR_BelgiqueURC_Text[20] = "Masquer les bulles-infos des URs ayant une description."; //"hide 0 comments with descriptions";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[20] = "Masquer les URs avec descriptions mais sans commentaire."; //"Hide URs that have descriptions and zero comments";

window.UrcommentsNLFR_BelgiqueURC_Text[21] = "Masquer les URs fermés."; //"Hide Closed URs";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[21] = "Masquer les URs qui ont été fermés."; //"Hide closed URs";

window.UrcommentsNLFR_BelgiqueURC_Text[22] = "[URO+] Masquer les URs tagguées."; //"Hide Tagged URs";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[22] = "Masquer les URs qui sont marqués avec des tags de style URO+."; //"Hide URs that are tagged with URO+ style tags ex. [NOTE]";

window.UrcommentsNLFR_BelgiqueURC_Text[23] = "Jours de rappel : "; //"Reminder days: ";

window.UrcommentsNLFR_BelgiqueURC_Text[24] = "Jours de fermeture : "; //"Close days: ";

//settings tab
window.UrcommentsNLFR_BelgiqueURC_Text[25] = "Définir automatiquement le commentaire adéquat."; //"Auto set new UR comment";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[25] = "cette option défini automatiquement l'UR-commentaire sur les nouveaux URs qui n'ont pas encore reçu de commentaires."; //"Auto set the UR comment on new URs that do not already have comments";

window.UrcommentsNLFR_BelgiqueURC_Text[26] = "Rappel automatique."; //"Auto set reminder UR comment";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[26] = "Cette option défini automatiquement le commentaire de rappel pour les URs qui sont plus anciens que la configuration des jours de rappel et qui ne comportent qu'un seul commentaire."; //"Auto set the UR reminder comment for URs that are older than reminder days setting and have only one comment";

window.UrcommentsNLFR_BelgiqueURC_Text[27] = "Zoom automatique sur les nouveaux URs."; //"Auto zoom in on new UR";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[27] = "Cette option permet d'effectuer un zoom automatique lors de l'ouverture des URs sans commentaires lors de l'envoi du rappel."; //"Auto zoom in when opening URs with no comments and when sending UR reminders";

window.UrcommentsNLFR_BelgiqueURC_Text[28] = "Centrer automatiquement l'UR."; //"Auto center on UR"
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[28] = "Cette option permet de centrer automatiquement la carte sur le zoom actuel lorsque qu'un UR a des commentaires et que le zoom est inférieur à 15."; //"Auto Center the map at the current map zoom when UR has comments and the zoom is less than 3";

window.UrcommentsNLFR_BelgiqueURC_Text[29] = "clic automatique \"ouvert\", \"résolu\", \"non identifé\"."; //"Auto click open, solved, not identified";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[29] = "Cette option sélectionne l'option \"résolu\" ou \"non identifié\" correspondant à la réponse automatique."; //"Suppress the message about recent pending questions to the reporter and then depending on the choice set for that comment Clicks Open, Solved, Not Identified";

window.UrcommentsNLFR_BelgiqueURC_Text[30] = "Sauvegarde automatique après un commentaire résolu ou non identifié."; //"Auto save after a solved or not identified comment";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[30] = "Si le clic automatique \"ouvert\", \"résolu\", \"non identifé\" est également vérifié, cette option cliquera sur le bouton de sauvegarde (après avoir cliqué sur un commentaire d'UR et ensuite sur le bouton d'envoi)."; //"If Auto Click Open, Solved, Not Identified is also checked, this option will click the save button after clicking on a UR-Comment and then the send button";

window.UrcommentsNLFR_BelgiqueURC_Text[31] = "Fermeture automatique de la fenêtre \"commentaire\""; //"Auto close comment window";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[31] = "Pour les demandes ne nécessitant pas d'enregistrement, cette option fermera la requête de l'utilisateur après avoir cliqué sur un commentaire et sur le bouton d'envoi."; //"For the user requests that do not require saving this will close the user request after clicking on a UR-Comment and then the send button";

window.UrcommentsNLFR_BelgiqueURC_Text[32] = "Recharger la carte automatiquement après un commentaire."; //"Auto reload map after comment"
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[32] = "Cette option recharge la carte automatiquement après avoir choisi un \"UR-Comment\" puis sur \"Envoyer\". Ceci ne s'applique pas aux messages qui doivent être sauvegardés car la sauvegarde recharge automatiquement la carte. Actuellement cette option n'est pas nécessaire mais laissée en cas de modifications."; //"Reloads the map after clicking on a UR-Comment and then send button. This does not apply to any messages that needs to be saved, since saving automatically reloads the map. Currently this is not needed but I am leaving it in encase Waze makes changes";

window.UrcommentsNLFR_BelgiqueURC_Text[33] = "Zoom arrière automatique après commentaire."; //"Auto zoom out after comment";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[33] = "Après avoir cliqué sur un UR-commentaire dans la liste et cliqué sur (envoyer), le zoom de la carte sera rétabli à votre zoom précédent."; //"After clicking on a UR-Comment in the list and clicking send on the UR the map zoom will be set back to your previous zoom";

window.UrcommentsNLFR_BelgiqueURC_Text[34] = "Passer automatiquement à l'onglet \"Commentaires\"."; //"Auto switch to the UrComments tab";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[34] = "Cette option passe automatiquement à l'onglet \"Commentaires\" lors de l'ouverture d'un UR. Lorsque la fenêtre de l'UR est fermée, vous passerez directement à l'onglet précédent."; //"Auto switch to the URComments tab when opening a UR, when the UR window is closed you will be switched to your previous tab";

window.UrcommentsNLFR_BelgiqueURC_Text[35] = "Double clic pour envoi automatique."; //"Close message - double click link (auto send)";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[35] = "Cette option permet en double-cliquant sur un UR-Commentaire d'ajouter le texte dans l'UR et de l'envoyer automatiquement."; //"Add an extra link to the close comment when double clicked will auto send the comment to the UR windows and click send, and then will launch all of the other options that are enabled";

window.UrcommentsNLFR_BelgiqueURC_Text[36] = "Tous les commentaires - Double-cliquez sur le lien (envoi automatique)."; //"All comments - double click link (auto send)";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[36] = "(à développer) Ajoute un lien supplémentaire à chaque commentaire de la liste qui, en double-cliquant, enverra automatiquement le commentaire aux fenêtres UR et cliquera sur envoyer. Puis toutes les autres options activées seront lancées."; //Add an extra link to each comment in the list that when double clicked will auto send the comment to the UR windows and click send, and then will launch all of the other options that are enabled";

window.UrcommentsNLFR_BelgiqueURC_Text[37] = "Choix de la langue :"; //"Comment List";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[37] = "Ce choix multiple permet de choisir votre langue. Si vous souhaitez que votre liste de commentaires soit intégrée dans ce script ou que vous avez des suggestions à faire, veuillez me contacter sur Waze (@tunisiano187)."; //This shows the selected comment list. There is support for a custom list. If you would like your comment list built into this script or have suggestions on the Comments team’s list, please contact me at rickzabel @waze or @gmail";

window.UrcommentsNLFR_BelgiqueURC_Text[38] = "Désactiver les boutons \"Résolu\" et \"suivant\"."; //"Disable done / next buttons";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[38] = "Cette option permet de désactiver les boutons \"Résolu\" et \"suivant\" au bas de la nouvelle fenêtre."; //"Disable the done / next buttons at the bottom of the new UR window";

window.UrcommentsNLFR_BelgiqueURC_Text[39] = "Ne plus suivre l'UR après l'envoi."; //"Unfollow UR after send";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[39] = "Cette option permet de ne plus suivre un UR après avoir envoyé un commentaire."; //"Unfollow UR after sending comment";

window.UrcommentsNLFR_BelgiqueURC_Text[40] = "Envoi automatique de rappels."; //"Auto send reminders";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[40] = "Cette option permet d'envoyer automatiquement des rappels à tous les URs suivis au délai dépassé."; //"Auto send reminders to my UR as they are on screen";

window.UrcommentsNLFR_BelgiqueURC_Text[41] = "Bulle-info / Nom de l'éditeur."; //"Replace tag name with editor name";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[41] = "Cette option permet de remplacer le nombre de commentaires de la bulle-info par le nom du dernier éditeur."; //"When a UR has the logged in editors name in the description or any of the comments of the UR (not the name Waze automatically add when commenting) replace the tag type with the editors name";

window.UrcommentsNLFR_BelgiqueURC_Text[42] = "(Double cliquer pour fermer les liens)"; //double click to close links
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[42] = "Double cliquez ici pour un envoi automatique."; //"Double click here to auto send - ";

window.UrcommentsNLFR_BelgiqueURC_Text[43] = "[URO+] Ne pas afficher la bulle-infos sous les URs."; //"Dont show tag name on pill";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[43] = "Cette option permet de ne pas montrer la bulle-info sous les URs où il y a une étiquette URO."; //"Dont show tag name on pill where there is a URO tag";

window.UrcommentsNLFR_BelgiqueURC_Text[44] = "Signature"; //"Signature";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[44] = "Cette option vous permet de définir signature personnalisé qui sera automatiquement affichée sous les textes des UR-Commentaire. Après avoir apporté une modifications, veuillez actualiser votre page pour mettre à jour votre nouveau texte"; //"Here you can set your signature, after making a change, please refresh your page to update the texts";

window.UrcommentsNLFR_BelgiqueURC_Text[45] = "c";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[45] = "Commentaires sur cette demande";

window.UrcommentsNLFR_BelgiqueURC_Text[46] = "j";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[46] = "Jours depuis le dernier commentaire";

window.UrcommentsNLFR_BelgiqueURC_Text[47] = "Pour tout contact";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[47] = "Pour tout contact";

window.UrcommentsNLFR_BelgiqueURC_Text[48] = "Développement";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[48] = "Développeur";

window.UrcommentsNLFR_BelgiqueURC_Text[49] = "Textes et commentaires";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[49] = "Traduction et commentaires";

window.UrcommentsNLFR_BelgiqueURC_Text[50] = "Inclure un Post-scriptum"
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[50] = "Cette option vous permet d'inclure un Post-scriptum sous vos réponses";

window.UrcommentsNLFR_BelgiqueURC_Text[51] = "PS";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[51] = "Post-scriptum";

window.UrcommentsNLFR_BelgiqueURC_Text[52] = "Ces options sont actuellement en test";

window.UrcommentsNLFR_BelgiqueURC_Text[53] = "Mainteneur";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[53] = "Entretenu par";

window.UrcommentsNLFR_BelgiqueURC_Text[54] = "Salutation";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[54] = "Définissez un message d'accueil personnalisé, celui-ci sera placé avant la réponse configurée";

window.UrcommentsNLFR_BelgiqueURC_Text[55] = "Masquer la description d'AndroidAuto";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[55] = "Masque la description 'Reported from AAOS'";

window.UrcommentsNLFR_BelgiqueURC_Text[56] = "+ Nouvelle liste";

window.UrcommentsNLFR_BelgiqueURC_Text[57] = "Liste de réponses";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[57] = "Utiliser une liste de réponses personnalisée";

window.UrcommentsNLFR_BelgiqueURC_Text[58] = "Résponses";

window.UrcommentsNLFR_BelgiqueURC_Text[59] = "Modifier la réponse personnalisée";
window.UrcommentsNLFR_BelgiqueURC_Text[60] = "Ajouter une réponse personnalisée";

window.UrcommentsNLFR_BelgiqueURC_Text[61] = "Titre";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[61] = "Entrez le titre de l'option, pour le remplissage automatique, cela doit correspondre à UR type";

window.UrcommentsNLFR_BelgiqueURC_Text[62] = "Réponse";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[62] = "Entrez le message pour répondre à l'UR";

window.UrcommentsNLFR_BelgiqueURC_Text[63] = "Statut de l'UR";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[63] = "Sélectionnez le statut à appliquer lorsque vous cliquez sur cette option";

window.UrcommentsNLFR_BelgiqueURC_Text[64] = "Signature";
window.UrcommentsNLFR_BelgiqueURC_Text_tooltip[64] = "Ajoutez votre signature à la réponse";

window.UrcommentsNLFR_BelgiqueURC_Text[65] = "Sauvegarder";
window.UrcommentsNLFR_BelgiqueURC_Text[66] = "Annuler";

window.UrcommentsNLFR_BelgiqueURC_Text[67] = "Options de liste personnalisées";
window.UrcommentsNLFR_BelgiqueURC_Text[68] = "Ajouter une réponse";
window.UrcommentsNLFR_BelgiqueURC_Text[69] = "Modifier une réponse";
window.UrcommentsNLFR_BelgiqueURC_Text[70] = "Modifier le nom de la liste";
window.UrcommentsNLFR_BelgiqueURC_Text[71] = "ZONE DANGEREUSE";
window.UrcommentsNLFR_BelgiqueURC_Text[72] = "Supprimer cette liste";
window.UrcommentsNLFR_BelgiqueURC_Text[73] = "Annuler la suppression";
window.UrcommentsNLFR_BelgiqueURC_Text[74] = "Supprimer une réponse";

window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[0] = "UR-Commenaires - Vous avez une ancienne version ou votre fichier de traduction est corrompu. Une erreur de synthaxe peut également être à l'origine de l'erreur. Disparu : ";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[1] = "UR-Commentaires - Il vous manque les éléments suivants de votre liste de commentaires personnalisée : "; //"UR Comments - You are missing the following items from your custom comment list: ";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[2] = "La liste ne peut pas être trouvée, vous pouvez trouver la liste et les instructions en contactant : @tunisiano187"; //"List can not be found you can find the list and instructions at https://wiki.waze.com/wiki/User:Rickzabel/UrComments/";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[3] = "UR-Commentaires - Vous ne pouvez pas régler la fermeture à zéro jours !";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[4] = "UR-Commentaires - Pour utiliser les liens de double-clic, vous devez avoir l'option Auto-clic \"ouvert\", \"résolu\", \"non identifé\"."; //"URComments - To use the double click links you must have the Auto click open, solved, not identified option enabled";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[5] = "UR-Commentaires - Abandonner \"FilterURs2\" car les filtrages, les comptages et les rappels automatiques sont désactivés."; //"URComments - Aborting FilterURs2 because both filtering, counts, and auto reminders are disabled";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[6] = "UR-Commentaires : Le chargement des informations de l'UR à expiré - Nouvelle tentative."; //this message is shown across the top of the map in a orange box, length must be kept short
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[7] = "UR-Commentaires: Ajout du message de rappel à l'UR :"; //this message is shown across the top of the map in a orange box, length must be kept short
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[8] = "Le filtrage des UR-Commentaires a été désactivé car les filtres UR de URO+ sont actifs."; // "URComment's UR Filtering has been disabled because URO+\'s UR filters are active."; //this message is shown across the top of the map in a orange box, length must be kept short
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[9] = "UR-Commentaires a détecté que vous avez des modifications non enregistrées!\n\nAvec l'option sauvegarde automatique activée et avec les modifications non enregistrées, vous ne pouvez pas envoyer de commentaires nécessitant l'enregistrement du script. Enregistrez vos modifications puis cliquez à nouveau sur le commentaire que vous souhaitez envoyer."; //"UrComments has detected that you have unsaved edits!\n\nWith the Auto Save option enabled and with unsaved edits you cannot send comments that would require the script to save. Please save your edits and then re-click the comment you wish to send.";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[10] = "UR-Commentaires : Impossible de trouver la boîte de commentaire ! Pour que ce script fonctionne, vous devez avoir un UR d'ouvert."; //"URComments: Can not find the comment box! In order for this script to work you need to have a UR open."; //this message is shown across the top of the map in a orange box, length must be kept short
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[11] = "UR-Commentaires - Ceci enverra des rappels lors du réglage des jours de rappel. Ceci ne se produit que lorsqu'ils sont dans votre zone visible.\n\nREMARQUE : Lorsque vous utilisez cette fonction, vous ne devez pas laisser d'UR ouvert à moins que vous ayez une question nécessitant une réponse du rapporteur. Ce script enverra ces rappels. "; //"URComments - This will send reminders at the reminder days setting. This only happens when they are in your visible area. NOTE: when using this feature you should not leave any UR open unless you had a question that needed an answer from the wazer as this script will send those reminders."; //conformation message/ question
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[12] = "URCom - Votre liste personnalisée configurée n'existe plus!";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[13] = "Ce nom existe déjà!";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[14] = "Nouvelle liste ajoutée avec succès";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[15] = "Échec de l'ajout d'une nouvelle liste!";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[16] = "Entrez un nouveau nom pour la liste";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[17] = "Nom mis à jour avec succès";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[18] = "Tous les champs obligatoires ne sont pas définis!";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[19] = "Échec de l'enregistrement du texte personnalisé!";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[20] = "Voulez-vous vraiment supprimer cette liste?";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[21] = "Échec de la suppression de la liste!";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[22] = "Cliquez sur l'option de texte pour supprimer";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[23] = "Échec de la suppression de l'élément!";
window.UrcommentsNLFR_BelgiqueURC_USER_PROMPT[24] = "Cliquez sur l'option de texte pour modifier";

//Le format des commentaires doit correspondre à ceci,
// "Titre",     * ce que vous verrez dans la liste des messages dans WME (doit correspondre aux textes des types d'UR Waze par défaut)
// "comment",   * ce qui sera envoyé au Wazer
// "URStatus"   * cette section correspond à l'état de votre UR après le click "Ouvert, Résolu, Non identifié". après avoir clisué sur envoyer, létat sera mis automatiquement. les possibilités sont. "Open", "Solved",ou "NotIdentified",
// Si vous souhaitez laisser une ligne vide entre les commentaires (menu) entrez les lignes suivantes
// "<br>",
// "",
// "",

window.UrcommentsNLFR_BelgiqueArray2 = [
    "Onduidelijke UR",
    "Merci pour votre signalement mais les informations fournies sont insuffisantes pour pouvoir corriger le problème. \n Pouvez-vous me donner plus d'informations svp? \nSi l'édition vous intéresse, rejoignez nous sur Slack: https://wazebelgium.be/join-slack/ \n\n Bedankt voor je melding.\nHelaas kunnen we er niet uit opmaken wat het probleem is. Wat kunnen we verbeteren? \n Ben je geïnteresseerd in het bewerken van de kaart? Sluit je hier aan: https://wazebelgium.be/join-slack/",
    "Open",
    "Herhaal beschrijving van de melder",
    "Bedankt voor je melding.\nJe schreef \"$URD\", maar we kunnen er helaas niet uit opmaken wat het probleem is. Wat kunnen we verbeteren?",
    "Open",
    "Melder volgt Waze route",
    "Bedankt voor je melding.\nVolgens de gegevens die wij kunnen zien volgde je de route die Waze voorstelde. Wat ging er mis?",
    "Open",
    "Herinnering",
    "Even een herinnering.\nGraag ontvangen we meer informatie over je melding, zodat we de kaart verder kunnen verbeteren. Als we binnen enkele dagen geen reactie ontvangen, dan gaan we er van uit dat alles goed staat en wordt de melding gesloten.",
    "Open",
    "Sluiten zonder reactie",
    "Helaas hebben we geen verdere reactie ontvangen. We kunnen het probleem daarom op dit moment niet verbeteren en sluiten de melding. Mocht je in een volgende rit nog iets tegenkomen, ontvangen we graag een nieuwe melding.",
    "NotIdentified",
	"Opgelost",
    "Dankzij jouw melding hebben we de kaart kunnen verbeteren. Het kan enkele dagen duren voordat de verandering is doorgevoerd in de navigatiekaart (check eventueel de 'INTL map tiles' updates op status.waze.com).\nBedankt voor je hulp.",
    "Solved",
    "Geen nadere reactie; Opgelost",
    "We hebben geen nadere reactie ontvangen, maar gaan er van uit dat de kaart goed is aangepast en sluiten deze melding. Mocht je in een volgende rit nog iets tegenkomen, ontvangen we graag een nieuwe melding.",
    "Solved",
	"SL toegevoegd",
	"We hebben de maximumsnelheid toegevoegd volgens je melding . Het kan enkele dagen duren voordat de verandering is doorgevoerd in de navigatiekaart (check eventueel de 'INTL map tiles' updates op status.waze.com).\nBedankt voor je hulp.",
	"Solved",
    "Bedank reactie, melding sluiten",
    "Bedankt voor je reactie.\nDeze melding wordt gesloten. Mocht je opnieuw problemen tegenkomen, dan ontvangen we graag een nieuwe melding.",
    "NotIdentified",
    "Reset UR commentaar en status",
    "",
    "Open",
    "<br>",
    "",
    "",
    "Diverse antwoorden",
    "#Title",
    "",
    "Afslag niet toegestaan (kruising onbekend)",
    "Bedankt voor je melding.\nHelaas kunnen we er niet uit opmaken over welke kruising het gaat. Welke afslag is niet toegestaan en waarom niet? Kun je de straatnamen van de kruising vermelden?\nAlvast bedankt.",
    "Open",
	"Verkeerslichten",
	"Bedankt voor je melding.\nEr zijn geen verkeerslichten opgenomen in de Waze, Roodlichtcamera's kan je wel.",
	"NotIdentified",
	"SL onwaarschijnlijk",
	"Dank je voor je antwoord.\nDe gerapporteerde $URD lijkt onwaarschijnlijk op deze locatie. Kun je deze verifieren? Bij voorbaat dank voor je hulp.",
    "Open",
	"SL al actief, open",
    "Bedankt voor je antwoord.\nDeze snelheidslimiet is al actief op de locatie van je melding. Wat ging er mis?",
    "Open",
	"SL al actief, sluiten",
	"Bedankt voor je antwoord.\nDeze snelheidslimiet is al actief op de locatie van je melding. Om continu de maximumsnelheid te zien, ga naar Settings > Snelheidsmeter > Toon maximumsnelheid. We wensen je veel rijplezier met Waze.",
	"NotIdentified",
	"SL werkzaamheden",
    "Dank je voor je antwoord.\nTijdens werkzaamheden wordt de snelheidslimiet op de kaart niet aangepast. We hebben namelijk geen mechanisme om dit terug ongedaan te maken op het (ongekende) einde van de werken. ",
    "NotIdentified",
	"SL variabel",
	"Bedankt voor je antwoord.\nOp deze plaats is een variabele snelheid van toepassing. Helaas kunnen we (nog) geen variabele snelheden verwerken en wordt alleen de maximum snelheid getoond. Deze melding wordt dan ook gesloten. Mocht je in een volgende rit nog iets tegenkomen, ontvangen we graag een nieuwe melding.",
	"NotIdentified",
    "Valide Route / gereden snelheid",
    "Bedankt voor je antwoord.\nWe hebben de route bekeken en kunnen geen afwijkingen in de kaart vinden. Waze berekent de snelste route op basis van de gemiddelde gereden snelheden en houdt daarbij rekening met tijdsspecifieke drukte, zoals spitsuur. Probeer de voorgestelde route eens een paar keer, wellicht dat die op dat tijdstip toch sneller is. Mocht dat niet zo zijn, dan leert Waze dat die route langzamer is en zal de snellere route de voorkeur krijgen.",
    "NotIdentified",
    "Omleidingen / Vreemde routes",
    "Bedankt voor je antwoord.\nWaze geeft (soms complexe) omleidingen als die beduidend sneller zijn om je bestemming te bereiken. Daarbij wordt rekening gehouden met gemiddelde gereden snelheden van de wegen op bepaalde tijden. Probeer de gesuggereerde route eens, wellicht dat die op dat tijdstip toch sneller is. Indien dat niet zo blijkt te zijn, leert Waze door de door jou gereden snelheid en krijgt de snellere route de voorkeur. ",
    "NotIdentified",
    "Mogelijk GPS probleem",
    "Bedankt voor je antwoord.\nWe kunnen in de omgeving van je melding geen afwijkingen in de kaart vinden. Het lijkt alsof je een GPS probleem had. Het GPS signaal wordt belemmerd door voertuigen en grote gebouwen. Bij sommige toestellen verzwakt het GPS-signaal als ze ouder worden. Zorg er voor dat je toestel zo min mogelijk wordt afgeschermd en test eventueel je GPS-signaal (met een app).",
    "NotIdentified",
    "Wegafsluiting of file",
    "Als een weg is geblokkeerd door file, gebruik dan Melding (de oranje button) > File, zodat Waze leert dat er langzaam verkeer en zo mogelijk een snellere route zal zoeken. Een volledige wegafsluiting kun je aangeven via: Melding > Afsluiting. Waze houdt daar onmiddelijk rekening mee en zal jou en andere Wazers omleiden. Als het om een langerdurende afsluiting gaat, dan ontvangen we graag zoveel mogelijk details (zover je die weet), zoals duur vd afsluiting en wat er precies is afgesloten. Alvast bedankt.",
    "Open",
    "Huisnummer aanpassing",
    "Bedankt voor je antwoord.\nWe hebben het huisnummer opnieuw laten registreren in Waze, waardoor het probleem verholpen zou moeten zijn. Het kan enkele dagen duren voordat de verandering is doorgevoerd in de navigatiekaart (check eventueel de updates: http://bit.ly/1zLMaoN). Het is dan wel nodig om je favorieten en eerdere zoekresultaten bij te werken. Dat kun je simpelweg doen door ze te verwijderen en opnieuw toe te voegen. Mocht je na de update nog steeds problemen ondervinden, zien we graag een nieuwe melding.",
    "Solved",
    "Foutieve locatie",
    "Bedankt voor je antwoord.\nWaze gebruikt gegevens uit verschillende zoekmachines; vermoedelijk leidden de coördinaten van de locatie niet naar de goede plek op de kaart. Om zelf de juiste locatie toe te voegen, ga naar Melding > Plaats. Als je een foto toevoegt, zorg dan dat er geen persoonlijke gegevens op staan. Je mag ook het juiste adres doorgeven (ev. met naam van de locatie), dan voeren wij die op de kaart in. Alvast bedankt.",
    "Open",
    "Text-To-Speech",
    "Mogelijk was er een probleem met de Text-to-Speech-cache. Type cc@tts in de zoekbalk van het navigatiemenu en klik op 'zoeken'. Je krijgt dan de melding dat de TTS cach is leeg gemaakt. Het kan handig zijn de volgende route in te laden als je wifi hebt, om een eventuele nieuwe download van straatnamen niet van je databundel af te laten gaan. Thanks.",
    "NotIdentified",
    "App bug",
    "Het lijkt een bug in the app zelf te zijn. Helaas kunnen wij daar als editors niets aan doen. Je kunt het eventueel melden via https://support.google.com/waze/answer/6276841.",
    "NotIdentified",
    "Update favorieten en geschiedenis",
    "Om correcties in een locatie te gebruiken, is het nodig je favorieten en zoekgeschiedenis bij te werken. Dat kun je simpelweg doen door ze te verwijderen en nieuwe (Waze) zoekresultaten toe te voegen. (zie het icoontje onderaan de lijst van zoekresultaten). Mocht je na de update nog steeds problemen ondervinden, dan zien we graag een nieuwe melding.",
    "NotIdentified",
    "Nieuwe weg asfalteren",
    "Bedankt voor je antwoord.\nJe kunt een nieuwe weg toevoegen door naar het menu van de meldingen te gaan en op het tabblad 'Asfalteren' te klikken. Rij over de nieuwe weg heen, klik daarna op de stoomwals en ´Asfalteren stoppen´.  Informatie over de weg kun je in een nieuwe melding kwijt (Ontbrekende weg). Alvast bedankt.",
    "Open",
    "Flitspaal toevoegen",
    "Bedankt voor je antwoord.\nJe kunt een flitspaal toevoegen vanuit de app. Klik op Melding (de oranje button) > Flitspaal en kies vervolgens Snelheid, Roodlicht of Nep. Een editor zal deze flitspaal vervolgens goedkeuren. Alvast bedankt.",
    "NotIdentified",
    "Bestemming opvragen",
    "Bedankt voor je antwoord.\nWaze laat geen gegevens zien over je start- of eindpunt. Kun je aangeven wat je als bestemming hebt ingevoerd, zodat we de route kunnen controleren? Alvast bedankt.",
    "Open",
    "Kaart vernieuwen",
    "Bedankt voor je antwoord.\nHet lijkt alsof er kaartinformatie in je toestel ontbrak. Je kunt handmatig de kaart vernieuwen. Ga naar Instellingen > Scherm & Kaart > Data overdracht > Vernieuw de kaart in mijn omgeving.",
    "Open",
    "<br>",
    "",
    "",
    "Standaard UR antwoorden",
    "#Title",
    "",
    "Foute afslag", //6
    "Bedankt voor je reactie\nWat was het probleem met deze afslag?",
    "Open",
    "Foutief adres", //7
    "Bedankt voor je reactie\nWaze laat geen gegevens zien over je start- of eindpunt. Om welk adres gaat het en wat is het probleem met dit adres? Alvast bedankt.",
    "Open",
    "Rotonde niet aanwezig", //9
    "Bedankt voor je melding.\nHet is voor ons onduidelijk waar de rotonde ontbreekt. Welke wegen zijn verbonden met de rotonde? Alvast bedankt voor de info.",
    "Open",
    "Algemene fout", //10
    "Bedankt voor je melding.\nHelaas kunnen we er niet uit opmaken wat het probleem is. Wat kunnen we verbeteren?",
    "Open",
    "Afslag niet toegestaan", //11
    "Bedankt voor je melding.\nKun je omschrijven waarom de afslag niet is toegestaan? Is dit een tijdelijke situatie (bijv. door wegwerkzaamheden) of is het definitief (bijv. doordat de aansluitende weg eenrichtingsverkeer is)? Alvast bedankt voor je feedback.",
    "Open",
    "Foute kruising", //12
    "Bedankt voor je melding.\nHelaas kunnen we er niet uit opmaken wat er mis is met de kruising. Kun je omschrijven wat er verbeterd kan worden? Indien mogelijk graag ook de straatnamen van de kruising. Alvast bedankt.",
    "Open",
    "Ontbrekende brug", //13
    "Bedankt voor je melding.\nWelke brug ontbreekt er? Ter info: wanneer je met hoge snelheid rijdt laat Waze bewust niet alle kaartdetails zien, zodat het beeld helder en duidelijk blijft.",
    "Open",
    "Verkeerde instructie", //14
    "Bedankt voor je melding.\nHelaas kunnen we er niet uit opmaken wat er verkeerd ging in de route. Wat kunnen we verbeteren?",
    "Open",
    "Ontbrekende afrit", //15
    "Bedankt voor je melding.\nWelke afrit ontbreekt er? Hoe meer informatie je kunt geven, zoals naam en een omschrijving van hoe de afrit ongeveer loopt, hoe beter de kaart wordt.",
    "Open",
    "Ontbrekende weg", //16
    "Bedankt voor je melding.\nWelke weg ontbreekt er? Hoe meer informatie je kunt geven, zoals naam, rijrichting of andere zaken die van toepassing zijn, hoe beter de kaart wordt.",
    "Open",
    "Ontbrekend herkenningspunt/Plaats", //18
    "Bedankt voor je melding.\nJe kunt zelf de juiste locatie toevoegen: ga naar Melding > Plaats. Als je een foto maakt, zorg dan dat er geen persoonlijke gegevens op staan. Je kunt ook het juiste adres doorgeven en graag zoveel mogelijk info over de locatie, dan voeren wij die op de kaart in. Alvast bedankt.",
    "Open",
	"Speed Limit", //23
    "Je hebt een melding mbt de maximum snelheid gemaakt. Zou je kunnen vermelden van waar tot waar deze limiet geldt? Is dit een tijdelijke wijziging (zoals bij wegwerkzaamheden), of een definitieve verandering? Alvast bedankt voor je hulp.",
    "Open"
];
